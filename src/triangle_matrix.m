function mat = triangle_matrix(size)
% TRIANGLE_MATRIX Creates a triangle matrix with the given size.
%
% Params:
%   'size': The size of the matrix.
% Returns:
%   'mat': A matrix of the given size with a triangle of ones.

if size <= 0
    error("Size must be positive");
end

mat = tril(ones(size));