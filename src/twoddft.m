% This function is purely to show the images needed in problem 2.2.
% It is not needed for anything other file in the project.
%
% Reads the phantom image in the data directory, performs a fourier
% transform on it and shows it. This function doesn't take any input
% arguments and doesn't output anything.

function twoddft ()
image = read_image("./data/phantom.png");
transformed_image = abs(fft2(image));
shifted_image = fftshift(transformed_image);
imshow(transformed_image);
