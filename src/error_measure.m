function err = error_measure(x, x_hat)
% ERROR_MEASURE calculates error of reconstruction by the formula
% err = |x - x_hat |_fro / |x|_fro
% 
% Params:
% 'x': ground truth image
% 'x_hat': Reconstructed image
% 
% Returns:
% 'err': Error measure

err = frobenius_norm(x - x_hat) / frobenius_norm(x);

end

function dist = frobenius_norm(mat)
%FROBENIUS_NORM helper function to calculate the frobenius norm of matrix
% ( is equivalent to norm(mat, 'fro') )
%
% Params:
% 'mat': M x N matrix
%
% Returns:
% 'dist': Frobenius norm of matrix

if ~ismatrix(mat)
    error('Invalid matrix dimensions');
end

dist = sqrt( sum( mat.^2, [1,2] ) );

end