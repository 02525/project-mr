function result = recon_volume(data, slices)
% RECON_VOLUME Reconstructs slices from frequency space.
%
% Params:
%   'data': The frequency data to reconstruct the slices from. An NxNxM1
%   array of doubles.
%   'slices': A one-dimensional array of integers describing the indeces
%   of the slices to reconstruct. OPTIONAL: defaults to one of every slice.
% Returns:
%   'result': An NxNxM2 array of doubles containing the reconstructed
%   slices. Contains as many slices as 'slices' contains ints in the same 
%   order.

data_size = size(data);

if ndims(data) > 3
    error("data must be a three dimensional array.");
elseif diff(data_size(1:2))
    error("Input slices must be square.");
end

num_input_slices = data_size(3);
slice_size = data_size(1);

% If only one argument is given, give slices its default value.
if nargin == 1
    slices = 1:num_input_slices;
elseif ~isvector(slices)
    error("slices must be one-dimensional.");
elseif any((slices < 1) + (slices > num_input_slices))
    error("Cannot reconstruct a slice not in the data.");
end

result = zeros(slice_size, slice_size, length(slices));

% Reconstrust a slice for each index in 'slices'.
for i = 1:length(slices)
    slice_index = slices(i);
    % Performs inverse dft on the slice and puts it into the output matrix.
    result(:,:,i) = ifft2(fftshift(data(:,:,slice_index)));
end
