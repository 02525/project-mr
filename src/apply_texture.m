function image = apply_texture(matrix, texture)
% APPLY_TEXTURE Creates an image with pixels from the texture where the
% matrix has ones and black where it has zeros.
%
% Params: 
%   'matrix': The shape to apply the texture to.
%   'texture': A heightxwidth matrix. The texture to apply. Must be no 
%   smaller than the matrix in all dimensions.
% Returns:
%   'image': A heightxwidth matrix. An image with pixels from the texture where the
%   matrix has ones and black where it has zeros.

% The number of dimensions where the matrix is larger than the texture.
if sum(size(matrix) > size(texture)) ~= 0
    error("Matrix must not be larger than texture");
end
[matrix_height, matrix_width] = size(matrix);
image = texture(1:matrix_height, 1:matrix_width).*matrix;