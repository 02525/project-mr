function sim_data = generate_simdata(K, texFiles)
% GENERATE_SIMDATA 
%
% Params:
%   'K': The dimensions of the resulting image.
%   'texFiles': OPTIONAL. The path to the directory containing the texture files.
%   Must contain at least 3 pngs and contain no pngs with a smaller size
%   the required image.
%
% Results:
%   'sim_data': An image of the given size with a randomly generated
%   square, triangle and circle, all with a texture from the given file, if
%   such a file is given.

% Assert that the diameter is a scalar and greater than zero
if ~isscalar(K) && K > 0
   error('Diameter is not a scalar greater than zero'); 
end

if nargin == 2 && (~isstring(texFiles) && ~ischar(texFiles))
    error("File path not valid");
end



% Create black image
sim_data = zeros(K,K);

% Get random length and positions for shapes
[size1, pos1] = generate_size_location(K);
[size2, pos2] = generate_size_location(K);
[size3, pos3] = generate_size_location(K);

% Generate shapes
triangle = 1/3 * triangle_matrix(size1);

square = 1/3 * ones(size2, size2);

circle = 1/3 * circle_matrix(size3);

% If textures are given
if nargin == 2
    % Create a list of all file names in texFiles that end with .png.
    files = dir(strcat(texFiles, "*.png"));
    files = string({files.name});
    if length(files) < 3
        error("There must be at least 3 textures");
    end
    
    % Get textures from the files.
    % I don't exactly know what UniformOutput=false does, but it failed
    % without. Sorry.
    textures = arrayfun(@(f) read_image(strcat(texFiles, f)), files, 'UniformOutput', false);
    
    % Find texture numbers for the shapes.
    % Generate a random order of numbers from 1 to the number of textures.
    texture_numbers = randperm(length(textures));
    % Take the first three.
    texture_numbers = texture_numbers(1:3);
    
    % Apply textures to the shapes. Use cell2mat to break the images out of
    % their cells.
    triangle = apply_texture(triangle, cell2mat(textures(texture_numbers(1))));
    square = apply_texture(square, cell2mat(textures(texture_numbers(2))));
    circle = apply_texture(circle, cell2mat(textures(texture_numbers(3))));
    
end

% Place shapes in result
sim_data = place_matrix_in_matrix(triangle, sim_data, pos1);
sim_data = place_matrix_in_matrix(square, sim_data, pos2);
sim_data = place_matrix_in_matrix(circle, sim_data, pos3);

end

function [size, pos] = generate_size_location(K)
% Generate length of shape (limited to max K/2)
size = randi([1,K/2]);

% Generate position of shape bounded by length and image size
pos = randi([1,K-size],2,1);

end