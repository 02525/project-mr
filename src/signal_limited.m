function limited_signal = signal_limited(signal, frac)
% SIGNAL_LIMITED Sets high frequencies' amplitudes to 0 so that only frac
% of the coefficients remain.
%
% This is basically a low-pass filter.
% 
% Params:
%   'signal': A dft signal. A 2d matrix.
%   'frac': What fraction of coefficients to keep.
%
% Returns:
%   'limited_signal': The resulting signal.

if ~isscalar(frac) || frac < 0 || frac > 1
    error("frac must be a number between 0 and 1 inclusive.");
end

% Diff returns false iff all elements are equal.
if ~ismatrix(signal) || diff(size(signal))
    error("signal must be a square matrix.");
end

% Get the size of the signal. Use ans to discard a dimension.
[signal_size, ans] = size(signal);

% Total area of the non-zero coefficients should be frac of the current. So
% the width of the non-zero area should be the sqrt of frac.
side_length = sqrt(frac) * signal_size;
% The thickness of the zero area around the kept coefficients.
border_thickness = round((signal_size-side_length)/2);

% Creates a matrix with ones in the kept area.
limited_signal = zeros(signal_size);
limited_signal(border_thickness+1:signal_size-border_thickness, ...
               border_thickness+1:signal_size-border_thickness) = 1;

limited_signal = signal.*limited_signal;