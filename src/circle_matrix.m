function mat = circle_matrix(d)
% CIRCLEMATRIX Generate a logical matrix of a circle with a given diameter
%
% Params:
% 'd' - diameter, scalar greater than zero
%
% Returns:
% 'mat' - logical matrix of a circle with the given radius

% Assert that the diameter is a scalar and greater than zero
if ~isscalar(d) && d > 0
   error('Diameter is not a scalar greater than zero'); 
end

% Get matricies of respectivly x and y indices for a d*d matrix
[x, y] = meshgrid(1:d, 1:d);

% Calculate logical matrix, where elements of logical true,
% are inside circle
mat = (y - d/2).^2 + (x - d/2).^2 < (d/2).^2;
