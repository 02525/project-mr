function main_2_6()
% Generate vector of noise percentages
N = 16;
tau = 0:16;

% Generate test image
image_size = 256;
test_image = generate_simdata(image_size);

% Instantiate image and error arrays
noisy_test_images = zeros(image_size, image_size, N);
error = zeros(N,1);

% For each noise percentage
for i = 1:N
    noisy_image = create_noisy_image(test_image, tau(i));
    noisy_test_images(:,:, i) = noisy_image;
    error(i) = error_measure(test_image, noisy_image);
end

%imshow(noisy_test_images(:,:,8));

%disp(abs(error));
%plot(1:N, abs(error));
montage(noisy_test_images);
end


function noisy_image = create_noisy_image(image, tau)

transformed_image = fft2(image);

noisy_transform = addnoise(transformed_image, tau);

noisy_image = ifft2(noisy_transform);

end