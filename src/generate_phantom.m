function im = generate_phantom(K, fileName)
% GENERATE_PHANTOM generates and saves "Shepp-Logan" phantom
%
% Params:
% 'K': Size of image in pixels
% 'filename': filename of image (will be saved in ../data/)
% 
% Returns:
% 'im': K x K image of Shepp-Logan phantom

% Assert that K is positive scalar
if ~isscalar(K) || K < 0
   error('K is not non-negative integer'); 
end

% Generates the "Modified Shepp-Logan" phantom
im = phantom(round(K));

% If fileName is suppplied save image
if nargin == 2
    if ~ischar(fileName) && ~isstring(fileName)
        error("Filename is not vaild");
    end
    path = "../data/" + fileName;
    imwrite(im, path);
end

end




